package com.hexaware.ems.model;

import java.time.LocalDate;

public class EmployeeTest {
	
	private long empId;
	private String empName;
	private String empEmail;
	private LocalDate doj;
	private EmployeeTest manager;
	
	
	
	public EmployeeTest(long empId, String empName, String empEmail, LocalDate doj, EmployeeTest manager) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.empEmail = empEmail;
		this.doj = doj;
		this.manager = manager;
	}
	public EmployeeTest(long empId, String empName, String empEmail, LocalDate doj) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.empEmail = empEmail;
		this.doj = doj;
	}
	public long getEmpId() {
		return empId;
	}
	public void setEmpId(long empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpEmail() {
		return empEmail;
	}
	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}
	public LocalDate getDoj() {
		return doj;
	}
	public void setDoj(LocalDate doj) {
		this.doj = doj;
	}
	public EmployeeTest getManager() {
		return manager;
	}
	public void setManager(EmployeeTest manager) {
		this.manager = manager;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((doj == null) ? 0 : doj.hashCode());
		result = prime * result + ((empEmail == null) ? 0 : empEmail.hashCode());
		result = prime * result + (int) (empId ^ (empId >>> 32));
		result = prime * result + ((empName == null) ? 0 : empName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeTest other = (EmployeeTest) obj;
		if (doj == null) {
			if (other.doj != null)
				return false;
		} else if (!doj.equals(other.doj))
			return false;
		if (empEmail == null) {
			if (other.empEmail != null)
				return false;
		} else if (!empEmail.equals(other.empEmail))
			return false;
		if (empId != other.empId)
			return false;
		if (empName == null) {
			if (other.empName != null)
				return false;
		} else if (!empName.equals(other.empName))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "EmployeeTest [empId=" + empId + ", empName=" + empName + ", empEmail=" + empEmail + ", doj=" + doj
				+ ", manager=" + manager + "]";
	}
	
	

}
