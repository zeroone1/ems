package com.hexaware.ems.model;
import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EmployeeModelTesting {
	EmployeeTest  employeeTest;
	@Before
	public void use() {
		employeeTest= new  EmployeeTest(12,"basant","basant@gmail.com",LocalDate.of(2020, 07, 21));
	}
	@Test
	public void testEmpId()
	{
		 employeeTest.setEmpId(13);
		// Assert.assertEquals(13, employeeTest.getEmpId());
		 Assert.assertNotEquals(15, employeeTest.getEmpId());
		 
	}
	
	@Test
	public void testEmpName()
	{
		 employeeTest.setEmpName("neha");
		// Assert.assertEquals("neha", employeeTest.getEmpName());
		 Assert.assertNotEquals("sneha", employeeTest.getEmpName());
		 
	}
	
	@Test
	public void testEmpEmail()
	{
		 employeeTest.setEmpEmail("neha@gmail.com");
		Assert.assertEquals("neha@gmail.com", employeeTest.getEmpEmail());
		 Assert.assertNotEquals("sneha@gmail.com", employeeTest.getEmpEmail());
		 
	}
	

	@Test
	public void testDoj()
	{
		 employeeTest.setDoj(LocalDate.of(2019, 07, 21));
		 Assert.assertEquals(LocalDate.of(2019, 07, 21), employeeTest.getDoj());
		// Assert.assertNotEquals(LocalDate.of(2020, 5, 21), employeeTest.getDoj());
		 
	}
	
	@Test
	public void testCons()
	{
		
		// Assert.assertEquals(12,employeeTest.getEmpId());
		 Assert.assertNotEquals(13,employeeTest.getEmpId());
	     
		// Assert.assertEquals("basant", employeeTest.getEmpName());
	     Assert.assertNotEquals("bassy", employeeTest.getEmpName());
	     
		 //Assert.assertEquals("basant@gmail.com", employeeTest.getEmpEmail());
	     Assert.assertNotEquals("bassy@gmail.com", employeeTest.getEmpEmail());

		// Assert.assertEquals("2020, 07, 21", employeeTest.getDoj());
	     Assert.assertNotEquals("2019-03-12", employeeTest.getDoj());
	  }
	
	@Test
	public void TestManager()
	{
		 EmployeeTest  manager= new  EmployeeTest(12,"basant","basant@gmail.com",LocalDate.of(2020, 07, 21));
		 EmployeeTest  manager1= new  EmployeeTest(12,"akanksha","akanksha@gmail.com",LocalDate.of(2010, 06, 11));
		 employeeTest.setManager(manager);
		// Assert.assertEquals(manager, employeeTest. getManager());
		 Assert.assertNotEquals(manager1, employeeTest.getManager());
	}
	
	@Test
	public void testHashCode() {
		EmployeeTest  employeeTest2= new  EmployeeTest(123,"basa","basant@gmaom",LocalDate.of(2020, 07, 21));
		EmployeeTest  employeeTest1=  employeeTest;
		
		
		//Assert.assertEquals(employeeTest,employeeTest1);
		Assert.assertNotEquals(employeeTest, employeeTest2);
		
	}
	
	@Test
	public void testToString() {
		EmployeeTest  employeeTest2= new  EmployeeTest(123,"basant","basant@gmail.com",LocalDate.of(2020, 07, 21));
		String s = employeeTest2.toString();
		//Assert.assertEquals(s,employeeTest.toString());
		Assert.assertNotEquals(s,employeeTest.toString());
		
		}
	}
