package com.hexaware.ems.service;

import java.util.Set;

import com.hexaware.ems.model.Employee;

public interface EmployeeService {
	
	public Employee save(Employee user);
	
	public Set<Employee> findAll();
	
	public Employee findById(long userId);
	
	public void deleteById(long userId);


}
